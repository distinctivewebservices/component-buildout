const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

const clientConfig = {
    entry: './src/index.js',
    devtool: 'source-map',
    watch: true,
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist', 'app')
    },
    module: {
        rules: [
            {
                test: /\.js[x]?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader'
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        { loader: 'css-loader' },
                        { loader: 'sass-loader' }
                    ]
                })
            },
            {
                test: /\.htm[l]$/,
                use: [{
                    loader: 'file-loader?name=../[name].html'
                }]
            },
            {
                test: /\.png$/,
                use: [{
                    loader: 'file-loader?name=../images/[name].png'
                }]
            }]
    },
    resolve: {
        alias: {
            vue$: 'vue/dist/vue.common.js'
        }
    },
    plugins: [
        new ExtractTextPlugin('../css/styles.css')
    ]
};

module.exports = clientConfig;


// const serverConfig = {
//     entry: './server.js',
//     target: 'node',
//     devtool: 'source-map',
//     externals: [nodeExternals()],
//     output: {
//         filename: 'server.bundle.js',
//         path: path.resolve(__dirname, 'dist', 'app')
//     },
//     module: {
//         rules: [
//             {
//                 test: /\.js[x]?$/, 
//                 exclude: /node_modules/, 
//                 use: 'babel-loader'
//             },
//             {
//                 test: /\.css$/,
//                 use: ExtractTextPlugin.extract({
//                     use: 'css-loader'
//                 })
//             },
//             {
//                 test: /\.scss$/,
//                 use: ExtractTextPlugin.extract({
//                     use: [
//                         { loader: 'css-loader' },
//                         { loader: 'sass-loader' }
//                     ]
//                 })
//             },
//             {
//                 test: /\.htm[l]$/,
//                 use: [{
//                     loader: 'file-loader?name=../[name].html'
//                 }]
//             },
//             {
//                 test: /\.png$/,
//                 use: [{
//                     loader: 'file-loader?name=../images/[name].png'
//                 }]
//             }]
//     },
//     resolve: {
//         alias: {
//             vue$: 'vue/dist/vue.common.js'
//         }
//     },
//     plugins: [
//         new ExtractTextPlugin('../css/styles.css')
//     ]
// };

// module.exports = [clientConfig, serverConfig];