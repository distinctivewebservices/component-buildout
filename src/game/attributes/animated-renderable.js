import BoundingBox from './../library/bounding-box';
import Renderable from './renderable';
import Attribute from './attribute';
import Animation from './../library/animation';
import AnimationFrame from './../library/animation-frame';
import Helpers from './../library/helpers';

/**
 * @summary A dictionary of animations. 
 * @typedef AnimationDictionary
 * @property {Animation} * - The animations. 
 */

/**
 * Enables graphical rendering to this game object.
 * @property {string} src - The URL of where to find the image to use for this attribute.
 * @property {boolean} scaleToFitFrame - Should the image be scaled to fit a larger or smaller frame? 
 * @property {boolean} constrainProportions - Is the image is scaled, should the aspect ratio be maintained?
 */
export default class AnimatedRenderable extends Renderable {


    /** @type {string} */
    get src() { return null; }
    set src(value) { }

    /** @type {boolean} */
    get scaleToFitFrame() { return null; }
    set scaleToFitFrame(value) { }

    /** @type {boolean} */
    get constrainProportions() { return null; }
    set constrainProportions(value) { }


    constructor() {
        super();


        let src = '';
        let scaleToFitFrame = false;
        let constrainProportions = false;
        /** @type {Animation} */
        let currentAnimation = null;
        let animations = {};
        /** @type {HTMLImageElement} */
        let image = null;


        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');


        // Override properties 
        Object.defineProperty(this, 'src', {
            get() { return src; },
            set(value) {
                src = value;
                setSpriteMap(value);
            }
        });

        Object.defineProperty(this, 'scaleToFitFrame', {
            get() { return scaleToFitFrame; },
            set(value) { scaleToFitFrame = (value !== null) ? value : false; }
        });

        Object.defineProperty(this, 'constrainProportions', {
            get() { return constrainProportions; },
            set(value) { constrainProportions = (value !== null) ? value : false; }
        });


        // Override the renderable .getFrame property 
        this.getFrame = () => {

            if (refreshDue()) {
                var bounding = this.gameObject.boundingBox;
                ctx.clearRect(0, 0, bounding.width, bounding.height);
                canvas.width = bounding.width;
                canvas.height = bounding.height;
                // Render the new frame onto the canvas 
                let frame = currentAnimation.getCurrentFrame();
                if (frame.bitmap !== null) {
                    let scaleFactor = getFrameScaleFactor(frame);
                    ctx.drawImage(frame.bitmap, 0, 0, frame.imageDimensions.width * scaleFactor, frame.imageDimensions.height * scaleFactor);
                }
            }
            return canvas;

        }

        /** @param {AnimationFrame} frame */
        let getFrameScaleFactor = (frame) => {
            if (this.scaleToFitFrame) {
                var srcDims = frame.imageDimensions;
                var destDims = this.gameObject.boundingBox;
                return Helpers.Image.scaleFactor.getProportionalFitFactor(srcDims.width, srcDims.height, destDims.width, destDims.height);
            }
            return 1;
        }


        let setSpriteMap = (imageSrc) => {
            if (imageSrc && imageSrc != '') {
                // Promise needs to be used to ensure we have correct 'this' context.
                let prom = new Promise((resolve, reject) => {
                    image = new Image();
                    image.onload = () => {
                        resolve();
                    }
                    image.src = imageSrc;
                });
                prom.then(() => {
                    refreshMap();
                });
            }
        }

        let refreshMap = () => {
            if (image) {
                var allFrames = extractFramesFromAllAnimationsToArray();
                // Execute the extractImageDataFromMap on all frames, it returns a promise as a resuly, map the returned promises to an array 
                var bitmapPromises = allFrames.map(frame => {
                    return Helpers.Image.cropImage(image, frame.mapOffset.x, frame.mapOffset.y, frame.imageDimensions.width, frame.imageDimensions.height);
                });
                // When all promises have finished it means we have all our bitmaps and can then assign them to the source frames 
                Promise.all(bitmapPromises).then(returnedBitmaps => {
                    returnedBitmaps.forEach((bitmap, index) => {
                        allFrames[index].bitmap = returnedBitmaps[index];
                    });
                });
            }
        };

        let extractFramesFromAllAnimationsToArray = () => {
            /** @type {AnimationFrame[]} */
            var allFrames = [];
            for (let key in animations) {
                animations[key].getFrames().map(frame => allFrames.push(frame));
            }
            return allFrames;
        };


        /**
         * @summary Adds a new animation to the object. 
         * @prop {string} key - Name or identifier of the animation.
         * @prop {Animation} animation - The animation to add.
         */
        this.addAnimation = (key, animation) => {
            // Add the new animation 
            animations[key] = animation;
            // Assign current animation if this is the default animation or these is not yet an animation assigned 
            if (key === 'default' || currentAnimation === null) currentAnimation = animation;
            refreshMap();
        }

        /**
         * @summary Changes the animation that is playing.
         * @prop {string} key - Name or identifier of the animation.
         */
        this.setAnimation = (key) => {
            currentAnimation = animations[key];
            currentAnimation.reset();
        }

        let refreshDue = () => {
            if (currentAnimation === null)
                return false;
            else
                return true;
            // return this.currentAnimation.isFrameDue();
        }

    }


}