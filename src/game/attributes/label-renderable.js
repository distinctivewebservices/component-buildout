import BoundingBox from './../library/bounding-box';
import Renderable from './renderable';
import Attribute from './attribute';

export default class LabelRenderable extends Renderable {


    /** @type {string} */
    get font() { return ''; }
    set font(value) { }
    /** @type {string} */
    get foreColor() { return ''; }
    set foreColor(value) { }
    /** @type {string} */
    get backColor() { return null; }
    set backColor(value) { }
    /** @type {string} */
    get text() { return null; }
    set text(value) { }


    constructor() {
        super();

        // Override properties 
        // font
        let font = '11px sans-serif';
        Object.defineProperty(this, 'font', {
            get() { return font; },
            set(value) { font = value; dirty = true; }
        });
        // foreColor
        let foreColor = '#000000';
        Object.defineProperty(this, 'foreColor', {
            get() { return foreColor; },
            set(value) { foreColor = value; dirty = true; }
        });
        // backColor
        let backColor = '#000000';
        Object.defineProperty(this, 'backColor', {
            get() { return backColor; },
            set(value) { backColor = value; dirty = true; }
        });
        // text
        let text = '';
        Object.defineProperty(this, 'text', {
            get() { return text; },
            set(value) { text = (value !== null) ? value : ''; dirty = true; }
        });

        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        let dirty = true;

        // Override the renderable .getFrame property 
        this.getFrame = () => {

            if (this.visible && dirty && this.text) {

                let textWidth = ctx.measureText(this.text);
                let textHeight = !isNaN(parseInt(ctx.font)) ? parseInt(this.font) : 0;

                canvas.width = (this.boundingBox.width === 0) ? textWidth.width : this.boundingBox.width;
                canvas.height = (this.boundingBox.height === 0) ? textHeight : this.boundingBox.height;

                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.font = this.font;
                ctx.fillStyle = this.foreColor;
                if (this.backColor != null) ctx.backColor = this.backColor;
                ctx.fillText(this.text, 0, textHeight);

                dirty = false;

            }

            return canvas;

        }
    }


}