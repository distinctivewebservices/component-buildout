import Helpers from './../library/helpers';
import Attribute from './attribute';
import GameObject from './../game-objects/game-object';
import BoundingBox from './../library/bounding-box';
import Rectangle from './../library/rectangle';

/**
 * Gives a game object the ability to collide with other collidable objects.
 * @prop {BoundingBox} boundingBox - Gets collision area bounding box.
 * @prop {Rectangle} boundingRectanglePercentages - Gets the bounding box sizes as a percentage of the size of the object.
 * @prop {number} absoluteTop - Gets the Y coordinate of the top of the bounding box relative to the top left coordinate of the playfield.
 * @prop {number} absoluteRight - Gets the X coordinate of the right of the bounding box relative to the top left coordinate of the playfield.
 * @prop {number} absoluteBottom - Gets the Y coordinate of the bottom of the bounding box relative to the top left coordinate of the playfield.
 * @prop {number} absoluteLeft - Gets the X coordinate of the left of the bounding box relative to the top left coordinate of the playfield.
 * @
 */
export default class Collidable extends Attribute {


    /** @type {BoundingBox} */
    get boundingBox() { return null; }

    /** @type {Rectangle} */
    get boundingRectanglePercentages() { return null; }

    /** @type {number} */
    get absoluteTop() { return 0; }
    /** @type {number} */
    get absoluteRight() { return 0; }
    /** @type {number} */
    get absoluteBottom() { return 0; }
    /** @type {number} */
    get absoluteLeft() { return 0; }


    constructor() {
        super();


        Object.defineProperty(this, 'name', {
            get() { return Attribute.Names.collidable; }
        });

        let boundingBox = new BoundingBox();
        Object.defineProperty(this, 'boundingBox', {
            get: () => { return boundingBox; }
        });

        let boundingRectanglePercentages = new Rectangle();
        Object.defineProperty(this, 'boundingRectanglePercentages', {
            get: () => { return boundingRectanglePercentages; }
        });
        boundingRectanglePercentages.onchange = (sender, args) => {
            // Update the PX
        }


        Object.defineProperty(this, 'absoluteTop', {
            get: () => { return this.gameObject.boundingBox.positionY + this.boundingBox.positionY; }
        });
        Object.defineProperty(this, 'absoluteRight', {
            get: () => { return this.gameObject.boundingBox.positionX + this.boundingBox.positionX + this.boundingBox.width; }
        });
        Object.defineProperty(this, 'absoluteBottom', {
            get: () => { return this.gameObject.boundingBox.positionY + this.boundingBox.positionY + this.boundingBox.height; }
        });
        Object.defineProperty(this, 'absoluteLeft', {
            get: () => { return this.gameObject.boundingBox.positionX + this.boundingBox.positionX; }
        });


        this.boundingBox.ondimensionschanged = () => {

        }

        this.ongameobjectsizechanged = (sender, args) => {

        }

    }


    /**
     * Tests for a collision with, and raises the oncollisionwith event if a collision occured. 
     * @param {Collidable} collidable - Collidable object to test. 
     */
    testForCollisionWith(collidable) {
        // Helpers.Collisions.ifThisObject(this)
        //     .collidesWith(collidable)
        //     .then((result) => {

        //         // Raise the collision event
        //         this.oncollisionwith(collidable.gameObject);
        //     });
    }

    /**
     * @typedef oncollisionwithArgs
     * @property {boolean} colliding - Are the elements colliding?
     * @property {boolean} overlapX - Does the X coordinate overlap?
     * @property {boolean} overlapY - Does the Y coordinate overlap?
     */
    /**
     * Callback, when the object has collided with something. 
     * @param {GameObject} sender
     * @param {oncollisionwithArgs} args - Collision details object.
     */
    oncollisionwith(sender, args) {
    };


}