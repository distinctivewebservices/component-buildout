import GameObject from './../game-objects/game-object';

/**
 * @typedef AttributeNames 
 * @property {string} renderable 
 * @property {string} movable 
 * @property {string} collidable 
 */

/**
 * An attribute is an ability that you can add to a game object.
 * @property {string} name - Name of the attribute.
 * @property {GameObject} gameObject - GameObject to which this attribute is assigned. 
 */
export default class Attribute {


    /**
     * Fired when the size is changed of the parent game object.
     * @param {GameObject} sender - the GameObject that the size change is associated with.
     * @param {Object} args - Arguments.
     */
    ongameobjectsizechanged(sender, args) { }


    /** @type {AttributeNames} */
    static get Names() {
        return {
            renderable: 'renderable',
            movable: 'movable',
            collidable: 'collidable'
        }
    }

    /** @type {string} */
    get name() { return ''; }

    /** @type {GameObject} */
    get gameObject() { return null; }

    constructor() {

        /**
         * @type {GameObject}
         */
        let gameObject = null;
        /**
         * Gets the parent GameObject to which this attribute belongs. 
         * @return {GameObject} - The GameObject to which this attribute belongs. 
         */
        this.getParent = () => {
            return gameObject;
        }
        /**
         * Sets the parent GameObject to which this attribute belongs. 
         * @param {GameObject} gameObject - GameObject to which this attribute belongs. 
         */
        this.setParent = (parentGameObject) => {
            gameObject = parentGameObject;
        }

        Object.defineProperty(this, 'gameObject', {
            get() { return gameObject; }
        });

    }
}