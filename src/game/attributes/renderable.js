import Attribute from './attribute';
import GameObject from './../game-objects/game-object';
import BoundingBox from './../library/bounding-box';
import Point2D from './../library/point-2d';

/**
 * @summary Represents an object that can be rendered to the screen. 
 * @property {BoundingBox} boundingBox - The bounding box that controls the rendering of this object. 
 * @property {Point2D} anchor - Anchor point for the object, affects transformations such as rotation. 
 * @property {number} rotation - Rotation in degrees. 
 * @property {number} zIndex - Render order, with higher being behind. 
 * @property {boolean} visible - Controls whether this renderable object is visible (only applies to this renderable element, not the game element). 
 */
export default class Renderable extends Attribute {

    onzindexchanged(sender, args) { };

    /** @type {BoundingBox} */
    get boundingBox() { return null; }
    /** @type {Point2D} */
    get anchor() { return null; }
    /** @type {number} */
    get rotation() { return null; }
    /** @type {boolean} */
    get visible() { return false; }
    set visible(value) { }

    constructor() {
        super();


        Object.defineProperty(this, 'name', {
            get() { return Attribute.Names.renderable; }
        });


        let boundingBox = new BoundingBox();
        Object.defineProperty(this, 'boundingBox', {
            get() { return boundingBox; }
        });

        let anchor = new Point2D();
        Object.defineProperty(this, 'anchor', {
            get() { return anchor; }
        });

        let rotation = 0;
        Object.defineProperty(this, 'rotation', {
            get() { return rotation; },
            set(value) { rotation = value; }
        });

        let visible = true;
        Object.defineProperty(this, 'visible', {
            get() { return visible; },
            set(value) { visible = value; }
        });


    }

    /**
     * Creates a canvas object containing the image that will be rendered for this object. 
     * @returns {HTMLCanvasElement} - Returns a canvas that contains the image data to be rendered. 
     */
    getFrame() { };
    
}