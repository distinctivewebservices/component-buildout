import BoundingBox from './../library/bounding-box';
import Renderable from './renderable';
import Attribute from './attribute';
import Animation from './../library/animation';
import AnimationFrame from './../library/animation-frame';
import Helpers from './../library/helpers';


/**
 * Represents an object that can be rendered as an image.
 * @property {string} imageDisplayMode - Must be 'default', 'single, 'stretch', 'tileHorizontally', 'tileVertically', 'tile' as defined in 'ImageDisplayMode' helper object.
 * @property {string} src - URL of the image.
 */
export default class ImageRenderable extends Renderable {


    static get ImageDisplayMode() {
        return {
            default: 'default',
            single: 'single',
            stretch: 'stretch',
            stretchProportionallyFit: 'stretchProportionallyFit',
            stretchProportionallyCrop: 'stretchProportionallyCrop',
            tileHorizontally: 'tileHorizontally',
            tileVertically: 'tileVertically',
            tile: 'tile'
        }
    }


    /** @type {string} */
    get src() { return null; }
    set src(value) { }

    /** @type {string} */
    get imageDisplayMode() { }
    set imageDisplayMode(value) { }


    constructor() {
        super();


        let imageDisplayMode = ImageRenderable.ImageDisplayMode.default;
        /** @type {(ImageBitmap|HTMLCanvasElement|HTMLImageElement)} */
        let imageData = null;
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');


        Object.defineProperty(this, 'src', {
            get() { return (imageData) ? imageData.src : null; },
            set(value) {
                this.setImage(value, null);
            }
        });

        Object.defineProperty(this, 'imageDisplayMode', {
            get() { return imageDisplayMode; },
            set(value) {
                if (ImageRenderable.ImageDisplayMode[value] !== undefined) {
                    imageDisplayMode = value;
                }
                else {
                    throw 'Unknown value for ImageDisplayMode.';
                }
            }
        });


        /**
         * Sets the image to use for this object. 
         * @param {string} src - The URL for the image to load. 
         * @param {BoundingBox} [cropBounds] - Optional. Provides the bounding area for the image. 
         */
        this.setImage = (src, cropBounds) => {
            var image = new Image();
            image.onload = () => {
                if (cropBounds) {
                    Helpers.Image.cropImageWithBounds(image, cropBounds).then(resultImageData => {
                        imageData = resultImageData;
                    });
                }
                else {
                    Helpers.Image.cropImage(image).then(resultImageData => {
                        imageData = resultImageData;
                    });
                }
            };
            image.src = src;
        }


        /** @returns {HTMLCanvasElement} - Returns a canvas that contains the image data to be rendered. */
        this.getFrame = () => {

            if (imageData && frameDue()) {

                var bounding = this.gameObject.boundingBox;
                ctx.clearRect(0, 0, bounding.width, bounding.height);
                canvas.width = bounding.width;
                canvas.height = bounding.height;

                let modes = ImageRenderable.ImageDisplayMode;
                if (this.imageDisplayMode === modes.default || this.imageDisplayMode === modes.single) {
                    // Display a single image at 0, 0
                    ctx.drawImage(imageData, 0, 0);
                }
                else if (this.imageDisplayMode === modes.stretch) {
                    // Stretch image 
                    ctx.drawImage(imageData, 0, 0, bounding.width, bounding.height);
                }
                else if (this.imageDisplayMode === modes.stretchProportionallyCrop) {
                    // Draw the image, the smallest dimension of the image will be at the same size as the largest dimension of the bounding box 
                    let factor = Helpers.Image.scaleFactor.getCropFitFactor(imageData.width, imageData.height, bounding.width, bounding.height);
                    ctx.drawImage(imageData, 0, 0, imageData.width * factor, imageData.height * factor);
                }
                else if (this.imageDisplayMode === modes.stretchProportionallyFit) {
                    // Draw the image, the smallest dimension of the image will be at the same size as the largest dimension of the bounding box 
                    let factor = Helpers.Image.scaleFactor.getProportionalFitFactor(imageData.width, imageData.height, bounding.width, bounding.height);
                    ctx.drawImage(imageData, 0, 0, imageData.width * factor, imageData.height * factor);
                }
                else if (this.imageDisplayMode === modes.tile) {
                    // Tile image 
                    for (let x = 0; x < bounding.width; x += imageData.width) {
                        for (let y = 0; y < bounding.height; y += imageData.height) {
                            ctx.drawImage(imageData, x, y);
                        }
                    }
                }
                else if (this.imageDisplayMode === modes.tileHorizontally) {
                    // Tile image horizontally, centre vertically 
                    let yPos = (bounding.height - imageData.height) / 2;
                    for (let x = 0; x < bounding.width; x += imageData.width) {
                        ctx.drawImage(imageData, x, yPos);
                    }
                }
                else if (this.imageDisplayMode === modes.tileVertically) {
                    // Tile image vertically, centre horizontally 
                    let xPos = (bounding.width - imageData.width) / 2;
                    for (let y = 0; y < bounding.height; y += imageData.height) {
                        ctx.drawImage(imageData, xPos, y);
                    }
                }
                else {
                    throw 'Unknown display mode.';
                }

            }

            return canvas;
        };


        let frameDue = () => {
            /** @todo Implement logic to save CPU cycles. */
            return true;
        }


    }

}
