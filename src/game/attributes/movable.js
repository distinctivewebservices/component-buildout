import GameObject from './../game-objects/game-object';
import Renderable from './../attributes/renderable';
import Attribute from './attribute';
import Helpers from './../library/helpers';

/**
 * Arguments for initialising a new movable. 
 * @typedef MovableArgs 
 * @property {number} gravity - The gravitational force applied to the object. 0 = none. 1 = Earth like. 
 * @property {number} frictionX - The amount of stopping speed on the player's X axis.
 * @property {number} frictionY - The amount of stopping speed on the player's Y axis.
 * @property {number} velocityX - Speed of movement in pixels per second. 0 = stopped. 
 * @property {number} velocityY - Speed of movement in pixels per second. 0 = stopped. 
 */

/**
 * Event arguments passed when the movable fires one of its update events. 
 * @typedef MovableUpdateEventArgs 
 * @property {number} timeSinceLastFrame - Time passed since the last update event occurred. 
 */

/**
 * Gives the game object the ability to be moved in the playfield. 
 * @property {number} gravity - The gravitational force applied to the object. 0 = none. 1 = Earth like. 
 * @property {number} frictionX - The amount of stopping speed on the player's X axis.
 * @property {number} frictionY - The amount of stopping speed on the player's Y axis.
 * @property {number} velocityX - Speed of movement in pixels per second. 0 = stopped. 
 * @property {number} velocityY - Speed of movement in pixels per second. 0 = stopped. 
 */
export default class Movable extends Attribute {


    /** @type {number} */
    get gravity() { return 1; }
    set gravity(value) { }

    /** @type {number} */
    get frictionX() { return 1; }
    set frictionX(value) { }

    /** @type {number} */
    get frictionY() { return 1; }
    set frictionY(value) { }

    /** @type {number} */
    get velocityX() { return 0; }
    set velocityX(value) { }

    /** @type {number} */
    get velocityY() { return 0; }
    set velocityY(value) { }

    /** 
     * Fired before the movement data for the object is updated.
     * @param {Movable~OnUpdateCallback} onpreupdate - Called just before the movement data for this object is calculated. 
     */
    onpreupdate(timePassed) { }
    // /** 
    //  * Fired after the movement data for the object is updated.
    //  * @param {MovableOnUpdateCallback} onpostupdate - Time passed since the last update even occurred. 
    //  */
    /** @type {MovableOnUpdateCallback} */
    onpostupdate(timePassed) { }


    /**
     * Initialises a new instance of a Movable attribute. 
     * @param {MovableArgs} args 
     */
    constructor(args) {
        super();

        let gravity = (!Helpers.isUndefined(args) && !Helpers.isUndefined(args.gravity)) ? args.gravity : 1;
        let frictionX = (!Helpers.isUndefined(args) && !Helpers.isUndefined(args.frictionX)) ? args.frictionX : 0;
        let frictionY = (!Helpers.isUndefined(args) && !Helpers.isUndefined(args.frictionY)) ? args.frictionY : 0;
        let velocityX = (!Helpers.isUndefined(args) && !Helpers.isUndefined(args.velocityX)) ? args.velocityX : 0;
        let velocityY = (!Helpers.isUndefined(args) && !Helpers.isUndefined(args.velocityY)) ? args.velocityY : 0;

        let lastUpdate = Date.now();

        { // Override properties 

            Object.defineProperty(this, 'name', {
                get() { return Attribute.Names.movable; }
            });

            Object.defineProperty(this, 'gravity', {
                get() { return gravity; },
                set(value) { gravity = value; }
            });

            Object.defineProperty(this, 'frictionX', {
                get() { return frictionX; },
                set(value) { frictionX = value; }
            });

            Object.defineProperty(this, 'frictionY', {
                get() { return frictionY; },
                set(value) { frictionY = value; }
            });

            Object.defineProperty(this, 'velocityX', {
                get() { return velocityX; },
                set(value) { velocityX = value; }
            });

            Object.defineProperty(this, 'velocityY', {
                get() { return velocityY; },
                set(value) { velocityY = value; }
            });

        }


        this.update = () => {

            // How much time passed since last update 
            let timePassedMs = Date.now() - lastUpdate;
            lastUpdate = Date.now();

            this.onpreupdate(this, { timeSinceLastFrameMs: timePassedMs });

            this.velocityY += this.gravity;
            if (this.frictionX !== 0 && this.velocityX !== 0) this.velocityX -= ((this.frictionX / 100000) * timePassedMs) * this.velocityX;
            if (this.frictionY !== 0 && this.velocityY !== 0) this.velocityY -= ((this.frictionY / 100000) * timePassedMs) * this.velocityY;

            if (this.gameObject.hasAttribute(Attribute.Names.renderable)) {
                let xtravelled = this.velocityX / 1000 * timePassedMs;
                let ytravelled = this.velocityY / 1000 * timePassedMs;
                this.gameObject.boundingBox.positionX += xtravelled;
                this.gameObject.boundingBox.positionY += ytravelled;
            }

            this.onpostupdate(this, { timeSinceLastFrameMs: timePassedMs });
        }


    }
}

/**
 * Callback is activated before the movement for the object is updated.
 * @callback Movable~OnUpdateCallback
 * @param {Number} timePassed - Time passed since the last update occurred. 
 */

