/**
 * Allows multiple objects to subscribe to a particular event. 
 */
export default class GameEvent {
    constructor() {

        let subscribers = [];

        /**
         * Subscribes a callback to the 
         */
        this.subscribe = (callback) => {
            if (subscribers.indexOf(callback) === -1) {
                subscribers.push(callback);
            }
        };

        /**
         * Removes a callback from the object.
         */
        this.unsubscribe = (callback) => {
            let index = subscribers.indexOf(callback); 
            if (index > -1) {
                subscribers = subscribers.splice(index, 1); 
            }
        };

        /**
         * Activates the subscribed callback. 
         */
        this.fire = (...args) => {
            subscribers.forEach(subscriber => {
                subscriber(...args); 
            }); 
        }

    }
}