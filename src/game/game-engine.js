import GameObject from './game-objects/game-object';
import LabelGameObject from './game-objects/label-game-object';
import GameLoop from './game-loop';
import Attribute from './attributes/attribute';
import Renderable from './attributes/renderable';
import Movable from './attributes/movable';
import Collidable from './attributes/collidable';
import Helpers from './library/helpers';
import Renderer from './library/renderer';
import Controller from './controllers/controller';
import GameEvent from './game-event';

/**
 * Useful debugging options. 
 * @typedef DebugOptions 
 * @property {boolean} drawAnchors - Draw game object anchor points. 
 * @property {boolean} drawBounding - Draw game object bounding boxes. 
 * @property {boolean} drawCollisionBoxes - Draw game object bounding boxes. 
 * @property {boolean} drawFps - Draw the frames per second?
 */


/**
 * @description Represents a game engine object. 
 * @property {number} fps - Current frames per second.
 */
export default class GameEngine {


    /**
     * Triggered at the beginning of an update cycle before any action is taken.
     * @param {Object} callback - Callback function to be called when the loop event is raised.
     */
    onUpdateInit(callback) { };

    /**
     * Triggered just before movement is calculated.
     * @param {Object} callback - Callback function to be called when the loop event is raised.
     */
    onUpdatePostCollisions(callback) { };

    /**
     * Triggered just after movement is calculated.
     * @param {Object} callback - Callback function to be called when the loop event is raised.
     */
    onUpdatePostMovement(callback) { };

    /**
     * Triggered at the end of a game update cycle.
     * @param {Object} callback - Callback function to be called when the loop event is raised.
     */
    onUpdateFinished(callback) { };


    /** @type {DebugOptions}  */
    get debugOptions() { return null; }
    /** @type {number} */
    get fps() { return null; }


    /**
     * Initialises a new instance of a game loop object.
     * @param {HTMLCanvasElement} canvas - canvas to use as the viewport.
     */
    constructor(canvas) {


        let $game = this;
        let $canvas = canvas;
        let $ctx = $canvas.getContext('2d');
        let $renderer = new Renderer($game, $canvas);
        let $loop = new GameLoop();


        Object.defineProperty(this, 'debugOptions', {
            get() { return debugOptions; }
        });

        Object.defineProperty(this, 'fps', {
            get() { return fpsCount; }
        });


        let $onUpdateInitEvent = new GameEvent();
        this.onUpdateInit = (callback) => {
            $onUpdateInitEvent.subscribe(callback);
        }
        let $onUpdatePostCollisionsEvent = new GameEvent();
        this.onUpdatePostCollisions = (callback) => {
            $onUpdatePostCollisionsEvent.subscribe(callback);
        }
        let $onUpdatePostMovementEvent = new GameEvent();
        this.onUpdatePostMovement = (callback) => {
            $onUpdatePostMovementEvent.subscribe(callback);
        }
        let $onUpdateFinishedEvent = new GameEvent();
        this.onUpdateFinished = (callback) => {
            $onUpdateFinishedEvent.subscribe(callback);
        }


        /** @type {DebugOptions} */
        let debugOptions = { drawAnchors: false, drawBounding: false, drawFps: false, drawCollisionBoxes: false };
        /** @type {GameObject[]} */
        let gameObjects = [];
        /** @type {Movable[]} */
        let movables = [];
        /** @type {Collidable[]} */
        let collidables = [];
        /** @type {Renderable[]} */
        let renderables = [];
        /** @type {Controller[]} */
        let controllers = [];
        let fpsCount = 0;


        /**
         * Adds an array of GameObjects to the game. 
         * @param {GameObject[]} gameObjects - Array of GameObjects to add to the game. 
         */
        this.addGameObjects = (gameObjects) => {
            gameObjects.forEach(gameObject => {
                $game.addGameObject(gameObject);
            });
        }
        /**
         * Adds a single GameObject to the game. 
         * @param {GameObject} gameObject - The game object to add to the game. 
         */
        this.addGameObject = (gameObject) => {
            gameObject.game = $game;
            gameObjects.push(gameObject);
            if (gameObject.hasAttribute(Attribute.Names.renderable)) {
                renderables.push(gameObject.getAttribute(Attribute.Names.renderable));
            }
            if (gameObject.hasAttribute(Attribute.Names.movable)) {
                movables.push(gameObject.getAttribute(Attribute.Names.movable));
            }
            if (gameObject.hasAttribute(Attribute.Names.collidable)) {
                collidables.push(gameObject.getAttribute(Attribute.Names.collidable));
            }
        }


        // Manage the game loop 

        /**
         * @description Starts the update loop 
         */
        this.startGame = () => {
            $loop.startLoop();
        };
        /**
         * @description Stops the update loop 
         */
        this.stopGame = () => {
            $loop.stopLoop();
        };


        $loop.onloop = (sender, args) => {
            fpsCount = 1000 / args.timePassed;
            $onUpdateInitEvent.fire();
            gameObjects.forEach(gameObject => {
                gameObject.onGameLoop($game);
            });
            testCollisions();
            $onUpdatePostCollisionsEvent.fire();
            updateMovement();
            $onUpdatePostMovementEvent.fire();
            renderables = renderables.sort((a, b) => a.gameObject.zIndex > b.gameObject.zIndex ? 1 : 0);
            $renderer.render(renderables);
            $onUpdateFinishedEvent.fire(); 
        }


        // Collisions 
        let testCollisions = () => {
            let untested = collidables.slice();
            while (untested.length > 0) {
                let first = untested.shift();;
                untested.forEach(second => {
                    // Test collisions from first to second
                    if (first.gameObject.hasAttribute(Attribute.Names.movable)) {
                        Helpers.Collisions.GameObject.if(first.gameObject).hasHit(second).onAnyAxis(() => {
                            first.oncollisionwith(second.gameObject);
                        });
                    }
                    // Test collisions from second to first 
                    if (second.gameObject.hasAttribute(Attribute.Names.movable)) {
                        Helpers.Collisions.GameObject.if(second.gameObject).hasHit(first).onAnyAxis(() => {
                            second.oncollisionwith(first.gameObject);
                        });
                    }
                });
            }
        };


        // Movement 

        let updateMovement = () => {
            movables.forEach(movable => {
                movable.update();
            });
        };


        /**
         * Attach controllers to the game. 
         * @param {Controller[]} controllersToAdd - Array of controllers to attach to the game. 
         */
        this.addControllers = (controllersToAdd) => {
            controllersToAdd.forEach(controller => {
                $game.addController(controller);
            });
        };

        /** 
         * Adds a controller to the game. 
         * @param {Controller} controller 
         */
        this.addController = (controllerToAdd) => {
            if (controllers.indexOf(controllerToAdd) === -1) {
                controllers.push(controllerToAdd);
            }
        };

        /**
         * Tests whether a specified controller command is active. 
         * @param {String} command - The command to check for. 
         */
        this.controllerCommandIsActive = (command) => {
            for (let c = 0; c < controllers.length; c++) {
                if (controllers[c].isCommandActive(command)) return true;
            }
            return false;
        };


    }


};
