import Controller from './controller';

export default class KeyboardController extends Controller {
    constructor() {
        super();


        let $this = this;

        // /** @type {String[]} */
        // let activeCommands = [];
        let activeCommands = Object.create({});
        let assignedCommands = Object.create({});


        /** @param {String} command */
        this.isCommandActive = (command) => {
            return activeCommands[command];
        }


        /**
         * Defines a command and it's key.
         */
        this.defineCommand = (command, key) => {
            if (!assignedCommands[key]) assignedCommands[key] = [];
            assignedCommands[key].push(command);
        }

        window.onkeydown = (e) => {
            if (assignedCommands[e.key]) {
                assignedCommands[e.key].forEach(command => {
                    if (!activeCommands[command]) {
                        activeCommands[command] = true;
                        $this.fireOnCommand(command);
                        $this.fireOnCommandActivated(command);
                    }
                });
            }
        }
        window.onkeyup = (e) => {
            if (assignedCommands[e.key]) {
                assignedCommands[e.key].forEach(command => {
                    if (activeCommands[command]) {
                        activeCommands[command] = false;
                        $this.fireOnCommandReleased(command); 
                    }
                });
            }
        }



    }
}

/**
 * Defines a command and its key.
 * @property {String} command - The command name being mapped.
 * @property {String} key - The key being assigned. 
 */