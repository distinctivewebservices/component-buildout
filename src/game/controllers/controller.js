import GameEvent from './../game-event';

/**
 * Represents a game controller. 
 * @property {DefaultCommandsObject} defaultCommands 
 */
export default class Controller {

    /**
     * @typedef DefaultCommandsObject 
     * @property {String} moveUp 
     * @property {String} moveDown 
     * @property {String} moveLeft 
     * @property {String} moveRight 
     * @property {String} jump 
     * @property {String} action1 
     * @property {String} action2 
     * @property {String} action3 
     */
    static get defaultCommands() {
        return {
            moveUp: 'moveUp',
            moveDown: 'moveDown',
            moveLeft: 'moveLeft',
            moveRight: 'moveRight',
            jump: 'jump',
            action1: 'action1',
            action2: 'action2',
            action3: 'action3'
        }
    }

    constructor() {

        let onCommandEvent = new GameEvent();
        let onCommandActivatedEvent = new GameEvent();
        let onCommandReleasedEvent = new GameEvent();

        /**
         * Callback fires whenever a command has been activated. 
         * @param {Object} callback - The callback to be invoked. 
         */
        this.onCommand = (callback) => {
            onCommandEvent.subscribe(callback);
        };
        /**
         * Callback fires whenever a command has been activated (shuch as a key or button held).
         * @param {Object} callback - The callback to be invoked. 
         */
        this.onCommandActivated = (callback) => {
            onCommandActivatedEvent.subscribe(callback);
        };
        /**
         * Callback fires whenever a command has been deactivated (shuch as a key or button being released).
         * @param {Object} callback - The callback to be invoked. 
         */
        this.onCommandReleased = (callback) => {
            onCommandReleasedEvent.subscribe(callback);
        };

        /**
         * Informs subscribers that the a command has been fired. 
         * @param {String} command - Name of the command. 
         */
        this.fireOnCommand = (command) => {
            onCommandEvent.fire(command);
        };
        /**
         * Informs subscribers that a command is active, such as a key being held. 
         * @param {String} command - Name of the command. 
         */
        this.fireOnCommandActivated = (command) => {
            onCommandActivatedEvent.fire(command);
        };
        /**
         * Informs subscribers that an active command has been released. 
         * @param {String} command - Name of the command. 
         */
        this.fireOnCommandReleased = (command) => {
            onCommandReleasedEvent.fire(command);
        };

    }

    /**
     * Gets whether a command is active, example a key is being pressed. 
     * @param {String} command - The command to check for.
     * @returns {boolean} 
     */
    isCommandActive(command) { }

}