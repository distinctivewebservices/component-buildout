import BoundingBox from './../library/bounding-box';
import Attribute from './../attributes/attribute';

/**
 * Any game object must be an instance of this class. 
 * @property {string} objectType - A way of identifying the object.
 * @property {BoundingBox} boundingBox - The box that represents this object's position on the playfield.
 * @property {number} zIndex - Render order, with higher being behind. 
 * @property {boolean} visible - Controls whether the game object is visible in the playfield (if the object has a renderable function). 
 * @property {[X:string]:any} properties - Dictionary of properties for the object. 
 */
export default class GameObject {


    /** @type {string} */
    get objectType() { return ''; }
    set objectType(value) { }
    /** @type {BoundingBox} */
    get boundingBox() { return new BoundingBox(); }
    /** @type {number} */
    get zIndex() { return null; }
    /** @type {boolean} */
    get visible() { return false; }
    set visible(value) { }
    /** @type {[X:string]:any} */
    get properties() { return null; }

    /**
     * This callback is fired once per game loop. 
     * @param {GameEngine} game - The game that called the gameLoop callback.
     */
    onGameLoop(game) { };


    constructor() {


        /** @type {Attribute[]} */
        let attributeList = {};


        let objectType = '';
        Object.defineProperty(this, 'objectType', {
            get() { return objectType; },
            set(value) { objectType = value; }
        });

        let boundingBox = new BoundingBox();
        Object.defineProperty(this, 'boundingBox', {
            get() { return boundingBox; }
        });
        this.boundingBox.ondimensionschanged = () => {
            for (let key in attributeList) {
                attributeList[key].ongameobjectsizechanged(this, {});
            }
        };

        let visible = 0;
        Object.defineProperty(this, 'visible', {
            get() { return visible; },
            set(value) { visible = value; }
        });

        let zIndex = 0;
        Object.defineProperty(this, 'zIndex', {
            get() { return zIndex; },
            set(value) { zIndex = value; }
        });

        let properties = {};
        Object.defineProperty(this, 'properties', {
            get() { return properties; }
        });


        /**
         * Returns a boolean on whether this object has an attribute. 
         * @param {string} name - Name of the attribute to check for. 
         * @returns {boolean} True if the GameObject has the attribute, otherwise false. 
         */
        this.hasAttribute = (name) => {
            return attributeList.hasOwnProperty(name);
        }
        /**
         * Retreives an attribute from the game object. 
         * @param {string} name - Name of the attribute to get. 
         * @returns {(Attribute|null)} Returns the requested attribute or null if it was not found.
         */
        this.getAttribute = (name) => {
            if (attributeList.hasOwnProperty(name))
                return attributeList[name];
            else
                return null;
        };
        /**
         * Bestows the GameObject with attributes. 
         * @param {Attribute[]} attributes - Array of attributes to add. 
         */
        this.addAttributes = (attributes) => {
            attributes.forEach(attribute => {
                this.addAttribute(attribute);
            });
        };
        /**
         * Bestows the GameObject with an attribute. 
         * @param {Attribute} attribute - Attribute to add. 
         */
        this.addAttribute = (attribute) => {
            attribute.setParent(this);
            attributeList[attribute.name] = attribute;
        };


    }
}