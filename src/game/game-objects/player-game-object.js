import './../assets/blob-map.png';
import GameEngine from './../game-engine';
import GameObject from './game-object';
import Attribute from './../attributes/attribute';
import Controller from './../controllers/controller';
import Animation from './../library/animation';
import AnimationFrame from './../library/animation-frame';
import Helpers from './../library/helpers';
import CollisionsHelper from './../library/helper-collisions';
import AnimatedRenderable from './../attributes/animated-renderable';
import Movable from './../attributes/movable';
import Collidable from './../attributes/collidable';
import BoundingBox from './../library/bounding-box';

/**
 * Arguments passed for creating a player game object.
 * @typedef {Object} PlayerGameObjectProps 
 * @property {number} positionX - X position relative to the parent game object. A value of 0 means auto.
 * @property {number} positionY - Y position relative to the parent game object. A value of 0 means auto.
 * @property {number} width - Label width in px. 
 * @property {number} height - Label height in px. 
 * @property {number} zIndex - Z-index ordering of the object compared to other renderables. 
 */

/**
 * @summary Represents a player game object.
 * @property {BoundingBox} collisionBoundingBox - X position relative to the parent game object. A value of 0 means auto.
 * @property {BoundingBox} animationBoundingBox - X position relative to the parent game object. A value of 0 means auto.
 * @property {Movable} movable - X position relative to the parent game object. A value of 0 means auto.
 */
export default class PlayerGameObject extends GameObject {


    /** @type {BoundingBox} */
    get collisionBoundingBox() { return null; }
    /** @type {BoundingBox} */
    get animationBoundingBox() { return null; }
    /** @type {Movable} */
    get movable() { return null; }


    /**
     * @param {PlayerGameObjectProps} props 
     */
    constructor(props) {
        super();

        let $playerGameObject = this;
        let animatable = new AnimatedRenderable();
        let movable = new Movable({ gravity: 0 });
        let playerCollidable = new Collidable();


        Object.defineProperty(this, 'collisionBoundingBox', {
            get() { return playerCollidable.boundingBox; }
        });
        Object.defineProperty(this, 'animationBoundingBox', {
            get() { return animatable.boundingBox; }
        });
        Object.defineProperty(this, 'movable', {
            get() { return movable; }
        });


        let animation = '';
        animatable.addAnimation('default', new Animation({
            frames: [
                new AnimationFrame({ timeMs: (1000 * 2.5), mapOffset: { x: 400, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 800, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 1200, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 1600, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 2000, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 2400, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 2800, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 2400, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 2000, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 1600, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 1200, y: 0 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.025), mapOffset: { x: 800, y: 0 }, imageDimensions: { width: 400, height: 400 } })
            ]
        }));
        animatable.addAnimation('walking', new Animation({
            frames: [
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 400, y: 400 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 800, y: 400 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 1200, y: 400 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 1600, y: 400 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 2000, y: 400 }, imageDimensions: { width: 400, height: 400 } })
            ]
        }));
        animatable.addAnimation('falling', new Animation({
            frames: [
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 2400, y: 800 }, imageDimensions: { width: 400, height: 400 } })
            ]
        }));
        animatable.addAnimation('jumping', new Animation({
            frames: [
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 1200, y: 800 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 1600, y: 800 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 2000, y: 800 }, imageDimensions: { width: 400, height: 400 } }),
                new AnimationFrame({ timeMs: (1000 * 0.1), mapOffset: { x: 2400, y: 800 }, imageDimensions: { width: 400, height: 400 }, nextFrame: 3 })
            ]
        }));
        animatable.src = 'images/blob-map.png';
        animatable.scaleToFitFrame = true;
        animatable.anchor.x = 50;
        animatable.anchor.y = 88;

        if (props.positionX) this.boundingBox.positionX = props.positionX;
        if (props.positionY) this.boundingBox.positionY = props.positionY;
        if (props.width) this.boundingBox.width = props.width;
        if (props.height) this.boundingBox.height = props.height;
        if (props.zIndex) this.zIndex = props.zIndex;

        movable.gravity = 4;
        movable.frictionX = 500;
        movable.onpreupdate = (sender, args) => {
        }
        movable.onpostupdate = (sender, args) => {
            // Movement post update callback 
            let newAnimation = '';
            if (movable.velocityY < -10)
                newAnimation = 'jumping';
            else if (movable.velocityY > 10)
                newAnimation = 'falling';
            else if ((movable.velocityX <= -1 || movable.velocityX >= 1) && (movable.velocityY > -10 && movable.velocityY < 10))
                newAnimation = 'walking';
            else
                newAnimation = 'default';
            console.log(`newAnimation ${movable.velocityY}`);
             if (animation !== newAnimation) {
                 animation = newAnimation;
                 animatable.setAnimation(animation);
             }

        };

        playerCollidable.boundingBox.positionX = 20;
        playerCollidable.boundingBox.positionY = 20;
        playerCollidable.boundingBox.width = 20;
        playerCollidable.boundingBox.height = 20;

        this.addAttributes([animatable, movable, playerCollidable]);


        /**
         * @param {GameObject} collidingGameObject
         */
        playerCollidable.oncollisionwith = (collidingGameObject) => {

            // When the object collides with a platform object 
            if (collidingGameObject.objectType === 'platform') {

                /** @type {Collidable} */ // The collidable object associated with the object we're colliding with 
                let platformCollidable = collidingGameObject.getAttribute(Attribute.Names.collidable);

                Helpers.Collisions.GameObject.if($playerGameObject).hasHit(platformCollidable).onTopSide(() => {
                    movable.velocityY = 0;
                    let playerBoundingBoxDistanceFromBottom = $playerGameObject.boundingBox.height - (playerCollidable.boundingBox.height + playerCollidable.boundingBox.positionY);
                    $playerGameObject.boundingBox.positionY = platformCollidable.absoluteTop - $playerGameObject.boundingBox.height + playerBoundingBoxDistanceFromBottom;
                });
                Helpers.Collisions.GameObject.if($playerGameObject).hasHit(platformCollidable).onBottomSide(() => {
                    movable.velocityY = 0;
                    $playerGameObject.boundingBox.positionY = platformCollidable.absoluteBottom - playerCollidable.boundingBox.positionX;
                });
                Helpers.Collisions.GameObject.if($playerGameObject).hasHit(platformCollidable).onLeftSide(() => {
                    movable.velocityX = 0;
                    let playerBoundingBoxDistanceFromRight = $playerGameObject.boundingBox.width - (playerCollidable.boundingBox.width + playerCollidable.boundingBox.positionX);
                    $playerGameObject.boundingBox.positionX = platformCollidable.absoluteLeft - $playerGameObject.boundingBox.width + playerBoundingBoxDistanceFromRight;
                });
                Helpers.Collisions.GameObject.if($playerGameObject).hasHit(platformCollidable).onRightSide(() => {
                    movable.velocityX = 0;
                    $playerGameObject.boundingBox.positionX = platformCollidable.absoluteRight - playerCollidable.boundingBox.positionX;
                });

            }

        }

        /** @param {GameEngine} game */
        this.onGameLoop = (game) => {
            console.log(game.controllerCommandIsActive(Controller.defaultCommands.moveLeft));
            if (game.controllerCommandIsActive(Controller.defaultCommands.moveLeft)) {
                movable.velocityX = -250;
            }
            if (game.controllerCommandIsActive(Controller.defaultCommands.moveRight)) {
                movable.velocityX = 250;
            }
        }

    }
}
