import './../assets/platform-default.png';
import GameObject from './game-object';
import ImageRenderable from './../attributes/image-renderable';
import Movable from './../attributes/movable';
import Collidable from './../attributes/collidable';
import Helpers from './../library/helpers';

/**
 * Arguments passed for creating a platform game object.
 * @typedef {Object} PlatformGameObjectProps 
 * @property {number} positionX - X position relative to the parent game object. A value of 0 means auto.
 * @property {number} positionY - Y position relative to the parent game object. A value of 0 means auto.
 * @property {number} width - Label width in px. 
 * @property {number} height - Label height in px. 
 * @property {number} zIndex - Z-index ordering of the object compared to other renderables. 
 * @property {string} [src] - URL for the image to load. 
 */

/**
 * @summary Represents a platform game object.
 */
export default class PlatformGameObject extends GameObject {
    /**
     * @param {PlatformGameObjectProps} props 
     */
    constructor(props) {
        super();

        this.objectType = 'platform';

        let imageRenderable = new ImageRenderable();
        imageRenderable.imageDisplayMode = ImageRenderable.ImageDisplayMode.tile;
        imageRenderable.src = !props.src ? 'images/platform-default.png' : props.src;

        let movable = new Movable({ gravity: 0 });

        if (props.positionX) this.boundingBox.positionX = props.positionX;
        if (props.positionY) this.boundingBox.positionY = props.positionY;
        if (props.width) this.boundingBox.width = props.width;
        if (props.height) this.boundingBox.height = props.height;
        if (props.zIndex) this.zIndex = props.zIndex;

        let collidable = new Collidable(); 
        collidable.boundingBox.positionX = 0; 
        collidable.boundingBox.positionY = 0; 
        collidable.boundingBox.width = this.boundingBox.width; 
        collidable.boundingBox.height = this.boundingBox.height; 

        this.addAttributes([imageRenderable, movable, collidable]);

    }
}
