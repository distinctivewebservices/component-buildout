import GameObject from './game-object';
import LabelRenderable from './../attributes/label-renderable'; 

/**
 * Arguments passed for creating a label game object.
 * @typedef {Object} LabelGameObjectProps 
 * @property {string} font - Font to use, specify as canvas font. 
 * @property {string} foreColor - Colour of the text to be rendered. 
 * @property {string} backColor - Colour of the text background. 
 * @property {string} text - The string to render. 
 * @property {number} positionX - X position relative to the parent game object. A value of 0 means auto.
 * @property {number} positionY - Y position relative to the parent game object. A value of 0 means auto.
 * @property {number} width - Label width in px. 
 * @property {number} height- Label height in px. 
 * @property {number} zIndex - Z-index ordering of the object compared to other renderables. 
 */

/**
 * @summary Represents a text label game object.
 */
export default class LabelGameObject extends GameObject {
    /**
     * @param {LabelGameObjectProps} props 
     */
    constructor(props) {
        super(); 

        let labelAttr = new LabelRenderable();

        if (props.font) labelAttr.font = props.font; 
        if (props.foreColor) labelAttr.foreColor = props.foreColor; 
        if (props.backColor) labelAttr.backColor = props.backColor; 
        if (props.text) labelAttr.text = props.text; 

        if (props.positionX) this.boundingBox.positionX = props.positionX; 
        if (props.positionY) this.boundingBox.positionY = props.positionY; 
        if (props.width) this.boundingBox.width = props.width; 
        if (props.height) this.boundingBox.height = props.height; 
        if (props.zIndex) this.boundingBox.zIndex = props.zIndex; 

        this.addAttribute(labelAttr);

    }
}