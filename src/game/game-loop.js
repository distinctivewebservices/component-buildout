/**
 * Arguments passed from the GameLoop object.
 * @typedef {Object} GameLoopOnLoopArgs 
 * @property {number} TimePassed - Number of milliseconds passed since last frame update. 
 */
/**
 * @summary Represents a simple object that triggers game updates. 
 * @property {number} targetFps - Number of frames per second that the game loop conforms to. 
 * @property {boolean} running - Is the loop running? 
 * @property {number} timeSinceLastUpdate - Amount of milliseconds since last loop.  
 */
export default class GameLoop {

    /**
     * @summary Called when a new loop has occurred. 
     * @arg {GameLoop} sender - The GameLoop that fired the event. 
     * @arg {GameLoopOnLoopArgs} args - Event arguments. 
     */
    onloop(sender, args) { }
    /**
     * @summary Called when the target FPS changes. 
     * @param {GameLoop} sender - GameLoop that the callback originated from.
     * @param {GameLoopOnLoopArgs} args - Arguments supplied with the callback. 
     */
    ontargetfpschange(sender, args) { }

    /** @type {number} */
    get targetFps() { return 0; }
    set targetFps(value) { }

    /** @type {boolean} */
    get running() { return false; }

    /**
     * @param {number} [targetFps] - Target frames per second for this object. Default = 60.
     */
    constructor(targetFps) {

        if (!targetFps || targetFps === null) targetFps = 60;
        let updateMs = (1000 / targetFps);
        let nextUpdateTime = Date.now();
        let running = false;


        Object.defineProperty(this, 'targetFps', {
            get() { return targetFps; },
            set(value) {
                if (value != targetFps) {
                    targetFps = value;
                    updateMs = (1000 / targetFps);
                    this.ontargetfpschange(this, {});
                }
            }
        });

        Object.defineProperty(this, 'running', {
            get() { return running; }
        });


        // The game loop 
        let loop = () => {
            if (running) {
                nextUpdateTime += updateMs;
                this.onloop(this, { timePassed: updateMs });
                let msTillNextUpdate = nextUpdateTime - Date.now();
                window.setTimeout(() => loop(), msTillNextUpdate);
            }
        };

        /**
         * @summary Starts the game loop. 
         */
        this.startLoop = () => {
            if (!running) {
                running = true;
                nextUpdateTime = Date.now();
                loop();
            }
        };

        /**
         * @summary Stops the game loop. 
         */
        this.stopLoop = () => {
            running = false;
        };

    }


}