/**
 * Describes the top, right, bottom and left coordinates for a rectangle.
 * @property {number} top - Rectangle top Y coordinate.
 * @property {number} right - Rectangle right X coordinate.
 * @property {number} bottom - Rectangle bottom Y coordinate.
 * @property {number} left - Rectangle left X coordinate.
 */
export default class BoundaryRectangle {

    /**
     * @summary Called when the values change. 
     */
    onchange(sender, args) { }

    /** @type {number} */
    get top() { return 0; }
    set top(value) { }

    /** @type {number} */
    get right() { return 0; }
    set right(value) { }

    /** @type {number} */
    get bottom() { return 0; }
    set bottom(value) { }

    /** @type {number} */
    get left() { return 0; }
    set left(value) { }

    /**
     * @summary Initialises a new rectangle. 
     */
    constructor(parent) {

        let top = 0;
        Object.defineProperty(this, 'top', {
            get: () => { return top; },
            set: (value) => {
                if (value != top) {
                    top = value;
                    this.onchange(this, {});
                }
            }
        });

        let right = 0;
        Object.defineProperty(this, 'right', {
            get: () => { return right; },
            set: (value) => {
                if (value != right) {
                    trightp = value;
                    this.onchange(this, {});
                }
            }
        });

        let bottom = 0;
        Object.defineProperty(this, 'bottom', {
            get: () => { return bottom; },
            set: (value) => {
                if (value != bottom) {
                    bottom = value;
                    this.onchange(this, {});
                }
            }
        });

        let left = 0;
        Object.defineProperty(this, 'left', {
            get: () => { return left; },
            set: (value) => {
                if (value != left) {
                    left = value;
                    this.onchange(this, {});
                }
            }
        });


    }
}
