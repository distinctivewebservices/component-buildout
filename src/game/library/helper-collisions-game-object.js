import BoundingBox from './bounding-box';
import GameObject from './../game-objects/game-object';
import Attribute from './../attributes/attribute';
import Collidable from './../attributes/collidable';
import Movable from './../attributes/movable';
import Helpers from './helpers';

export default class This {

    /** @param {GameObject} first */
    static if(first) {

        if (!first.hasAttribute(Attribute.Names.collidable) || !first.hasAttribute(Attribute.Names.movable)) throw `Input object must have '${Attribute.Names.movable}' and '${Attribute.Names.collidable}' characteristics.`;
        /** @type {Collidable} */
        let firstCollidable = first.getAttribute(Attribute.Names.collidable);
        /** @type {Movable} */
        let firstMovable = first.getAttribute(Attribute.Names.movable);

        return {
            /** @param {(Collidable|GameObject)} secondCollidable */
            hasHit(secondCollidable) {
                // If a game object was passed then extract its collidable component
                if (secondCollidable.hasAttribute && secondCollidable.hasAttribute(Attribute.Names.collidable))
                    secondCollidable = secondCollidable.getAttribute(Attribute.Names.collidable);
                return {
                    onAnyAxis(callback) {
                        if ((firstMovable.velocityY > 0 && Helpers.Math.isBetween(firstCollidable.absoluteBottom, secondCollidable.absoluteTop, secondCollidable.absoluteBottom)) ||
                            (firstMovable.velocityY < 0 && Helpers.Math.isBetween(firstCollidable.absoluteTop, secondCollidable.absoluteTop, secondCollidable.absoluteBottom)) ||
                            (firstMovable.velocityX > 0 && Helpers.Math.isBetween(firstCollidable.absoluteRight, secondCollidable.absoluteLeft, secondCollidable.absoluteRight)) ||
                            (firstMovable.velocityX < 0 && Helpers.Math.isBetween(firstCollidable.absoluteLeft, secondCollidable.absoluteLeft, secondCollidable.absoluteRight))
                        ) {
                            if (callback) callback();
                            return true;
                        }
                        else return false;
                    },
                    onTopSide(callback) {
                        if (firstMovable.velocityY > 0 && Helpers.Math.isBetween(secondCollidable.absoluteTop, firstCollidable.absoluteTop, firstCollidable.absoluteBottom)) {
                            if (callback) callback();
                            return true;
                        }
                        else return false;
                    },
                    onBottomSide(callback) {
                        if (firstMovable.velocityY < 0 && Helpers.Math.isBetween(secondCollidable.absoluteBottom, firstCollidable.absoluteTop, firstCollidable.absoluteBottom)) {
                            if (callback) callback();
                            return true;
                        }
                        else return false;
                    },
                    onLeftSide(callback) {
                        if (firstMovable.velocityX > 0 && Helpers.Math.isBetween(secondCollidable.absoluteLeft, firstCollidable.absoluteLeft, firstCollidable.absoluteRight)) {
                            if (callback) callback();
                            return true;
                        }
                        else return false;
                    },
                    onRightSide(callback) {
                        if (firstMovable.velocityX < 0 && Helpers.Math.isBetween(secondCollidable.absoluteRight, firstCollidable.absoluteLeft, firstCollidable.absoluteRight)) {
                            if (callback) callback();
                            return true;
                        }
                        else return false;
                    }
                }
            }
        }
    }


}