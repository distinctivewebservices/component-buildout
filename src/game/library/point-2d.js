/**
 * A 2 dimensional coordinate. 
 * @property {number} x The X coordinate. 
 * @property {number} y The y coordinate. 
 */
export default class Point2D {

    /**
     * @summary Called when the value of the point has changed. 
     */
    onchange(sender, args) { }

    /** @type {number} */
    get x() { return 0; }
    set x(value) { }

    /** @type {number} */
    get y() { return 0; }
    set y(value) { }

    /**
     * @summary Initialises a new point coordinate. 
     */
    constructor(x, y) {

        if (!x || x === null) x = 0;
        Object.defineProperty(this, 'x', {
            get: () => { return x; },
            set: (value) => {
                if (value != x) {
                    x = value;
                    this.onchange(this, {});
                }
            }
        });

        if (!y || y === null) y = 0;
        Object.defineProperty(this, 'y', {
            get: () => { return y; },
            set: (value) => {
                if (value != y) {
                    y = value;
                    this.onchange(this, {});
                }
            }
        });

    }
}
