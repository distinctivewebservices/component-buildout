import BoundingBox from './bounding-box'; 

/**
 * Provides utilities for testing bounding box conditions. 
 */
export default class This {

    /**
     * Test for a collision between two bounding boxes. 
     * @param {BoundingBox} boxA - The first box to test. 
     * @param {BoundingBox} boxB - The second box to test. 
     * @returns {boolean} true if colliding, false if not.
     */
    static collidesWith(boxA, boxB) {
        // Obtain the horizontal coordinates of both boxes start and finish 
        let min = { a: This.getAbsoluteX(boxA), b: This.getAbsoluteX(boxB) };
        let max = { a: min.a + boxA.width, b: min.b + boxB.width };
        // Test to see if the intersect, (boxesMaxX - boxesMinX) < (b)
        if (This.rangesIntersect(Math.min(min.a, min.b), Math.max(max.a, max.b), Math.min(boxA.width, boxB.width), Math.max(boxA.width, boxB.width))) {
            min = { a: This.getAbsoluteY(boxA), b: This.getAbsoluteY(boxB) };
            max = { a: min.a + boxA.height, b: min.b + boxB.height };
            return This.rangesIntersect(Math.min(min.a, min.b), Math.max(max.a, max.b), Math.min(boxA.width, boxB.width), Math.max(boxA.width, boxB.width));
        }
        return false;
    }

    /**
     * Tests whether two ranges intersect. 
     * @param {number} rangeMin 
     * @param {number} rangeMax 
     * @param {number} smallestSize 
     * @param {number} largestSize 
     * @returns {boolean} true if the ranges intersect, false if not.
     */
    static rangesIntersect(rangeMin, rangeMax, smallestSize, largestSize) {
        return (rangeMax - rangeMin) < (largestSize + smallestSize);
    }

    /**
     * @param {BoundingBox} box
     */
    static getAbsoluteX(box) {
        return This.
            getFlatHierachy(box).
            map(b => { return b.positionX; }).
            reduce((a, b) => { return a + b; }, 0);
    }

    /**
     * @param {BoundingBox} box
     */
    static getAbsoluteY(box) {
        return This.
            getFlatHierachy(box).
            map(b => { return b.positionY; }).
            reduce((a, b) => { return a + b; }, 0);
    }

    /**
     * Flattens the parent hierachy of This bounding box into an array containing This box and all parents. 
     * @param {BoundingBox} box The bounding box where the hierachy starts. 
     * @param {BoundingBox[]} [array] The array to add the boxes into. 
     * @returns {BoundingBox[]} Flattened array of all bounding boxes. 
     */
    static getFlatHierachy(box, array) {
        if (!array || array === null) array = [];
        if (box) {
            array.push(box);
            if (box.parent) array = This.getFlatHierachy(box.parent, array);
        }
        return array;
    }

}
