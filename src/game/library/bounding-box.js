import BoundingBoxHelper from './helper-bounding-box';
import Point2D from './point-2d';
import Dimensions2D from './dimensions-2d';

/**
 * @summary Describes a box.
 * @property {Point2D} position - Object representing the position of the box relative to it's parent bounding box.
 * @property {number} PositionX - Position of the X coordinate of the box relative to it's parent.
 * @property {number} PositionY - Position of the Y coordinate of the box relative to it's parent.
 * @property {Dimensions2D} dimensions - Object representing the width and height of the box.
 * @property {number} width - Width of the box.
 * @property {number} height - Height of the box.
 * @property {BoundingBox} parent - Parent of the box if it is nested.
 */
export default class BoundingBox {

    /**
     * @summary Called when the position of the bounding box has changed. 
     */
    onpositionchange(sender, args) { }
    /**
     * @summary Called when the dimensions of the bounding box have chnaged. 
     */
    ondimensionschanged(sender, args) { }

    /** @type {Point2D} */
    get position() { return new Point2D(); }
    set position(value) { }

    /** @type {number} */
    get positionX() { return 0; }
    set positionX(value) { }

    /** @type {number} */
    get positionY() { return 0; }
    set positionY(value) { }

    /** @type {Dimensions2D} */
    get dimensions() { return new Dimensions2D(); }
    set dimensions(value) { }

    /** @type {number} */
    get width() { return 0; }
    set width(value) { }

    /** @type {number} */
    get height() { return 0; }
    set height(value) { }

    /**
     * Gets any parent bounding box. 
     * @type {BoundingBox}
     */
    get parent() { }

    /**
     * @summary Initialises a new bounding box. 
     * @param {BoundingBox} parentBoundingBox Sets the parent bounding box that this one is part of. 
     */
    constructor(parent) {

        // Position properties 
        let position = new Point2D();
        position.onchange = (sender, args) => { this.onpositionchange(this, args); };

        Object.defineProperty(this, 'position', {
            get: () => { return position; },
            set: (value) => {
                if (value.x != position.x || value.y != position.y) {
                    position = value;
                    position.onchange = (sender, args) => { this.onpositionchange(this, args); };
                    this.onpositionchange(this, { handled: false });
                }
                else position = value;
            }
        });

        Object.defineProperty(this, 'positionX', {
            get: () => { return position.x; },
            set: (value) => {
                if (value != position.x) {
                    position.x = value;
                    this.onpositionchange(this, { handled: false });
                }
            }
        });

        Object.defineProperty(this, 'positionY', {
            get: () => { return position.y; },
            set: (value) => {
                if (value != position.y) {
                    position.y = value;
                    this.onpositionchange(this, { handled: false });
                }
            }
        });

        // Dimensions
        let dimensions = new Dimensions2D();
        dimensions.onchange = (sender, args) => { this.ondimensionschanged(this, args); };

        Object.defineProperty(this, 'dimensions', {
            get: () => { return dimensions; },
            set: (value) => {
                if (value.width != dimensions.width || value.height != dimensions.height) {
                    dimensions = value;
                    dimensions.onchange = (sender, args) => { this.ondimensionschanged(this, { handled: false })};
                    this.ondimensionschanged(this, { handled: false });
                }
                else dimensions = value;
            }
        });

        Object.defineProperty(this, 'width', {
            get: () => { return dimensions.width; },
            set: (value) => {
                if (value != dimensions.width) {
                    dimensions.width = value;
                    this.ondimensionschanged(this, { handled: false });
                }
            }
        });

        Object.defineProperty(this, 'height', {
            get: () => { return dimensions.height; },
            set: (value) => {
                if (value != dimensions.height) {
                    dimensions.height = value;
                    this.ondimensionschanged(this, { handled: false });
                }
            }
        });

        Object.defineProperty(this, 'parent', {
            get: () => { return parent; }
        });

    }
}
