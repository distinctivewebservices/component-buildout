import Point2D from './point-2d';
import Dimensions2D from './dimensions-2d';

/**
 * @summary Represents a single frame of an animation. 
 * @typedef AnimationFrameProps
 * @property {number} timeMs - time in milliseconds that the frame is displayed for. 
 * @property {Point2D} mapOffset - Position on the map where the frame begins. 
 * @property {number} mapOffset.x - Position on the map where the frame begins. 
 * @property {number} mapOffset.y - Position on the map where the frame begins. 
 * @property {Dimensions2D} imageDimensions - width in pixels of the data to take from the image map. 
 * @property {number} imageDimensions.width - width in pixels of the data to take from the image map. 
 * @property {number} imageDimensions.height - width in pixels of the data to take from the image map. 
 * @property {Point2D} imageOffset - render position offset relative the animation's bounding box. 
 * @property {number} imageOffset.x - render position offset relative the animation's bounding box. 
 * @property {number} imageOffset.y - render position offset relative the animation's bounding box. 
 * @property {number} nextFrame - index of the next frame to play after this frame completes. 
 */

/**
 * @summary Represents a single frame of an animation. 
 * @type {animationFrame}
 * @property {(ImageData|Canvas)} bitmap - bitmap content for the animation frame. 
 */
export default class AnimationFrame {

    /** @type {number} */
    get timeMs() { }
    set timeMs(value) { }

    /** @type {Point2D} */
    get mapOffset() { }
    set mapOffset(value) { }

    /** @type {Dimensions2D} */
    get imageDimensions() { }
    set imageDimensions(value) { }

    /** @type {Point2D} */
    get imageOffset() { }
    set imageOffset(value) { }

    /** @type {number} */
    get nextFrame() { }
    set nextFrame(value) { }

    /** @type {(ImageData|Canvas)} */
    get bitmap() { }
    set bitmap(value) { }

    /**
     * @param {AnimationFrameProps} props
     */
    constructor(props) {

        let timeMs = (props.timeMs ? props.timeMs : (1000 / 30));
        Object.defineProperty(this, 'timeMs', {
            get() { return timeMs; },
            set(value) { timeMs = value; }
        });

        let mapOffset = new Point2D(
            (props.mapOffset && props.mapOffset.x ? props.mapOffset.x : 0),
            (props.mapOffset && props.mapOffset.y ? props.mapOffset.y : 0)
        );
        Object.defineProperty(this, 'mapOffset', {
            get() { return mapOffset; },
            set(value) { mapOffset = value; }
        });

        let imageDimensions = new Dimensions2D(
            (props.imageDimensions && props.imageDimensions.width ? props.imageDimensions.width : 0),
            (props.imageDimensions && props.imageDimensions.height ? props.imageDimensions.height : 0)
        );
        Object.defineProperty(this, 'imageDimensions', {
            get() { return imageDimensions; },
            set(value) { imageDimensions = value; }
        });

        let imageOffset = new Point2D(
            (props.imageOffset && props.imageOffset.x ? props.imageOffset.x : 0),
            (props.imageOffset && props.imageOffset.y ? props.imageOffset.y : 0)
        );
        Object.defineProperty(this, 'imageOffset', {
            get() { return imageOffset; },
            set(value) { imageOffset = value; }
        });

        let nextFrame = (props && props.nextFrame ? props.nextFrame : null); 
        Object.defineProperty(this, 'nextFrame', {
            get() { return nextFrame; },
            set(value) { nextFrame = value; }
        });

        let bitmap = null;
        Object.defineProperty(this, 'bitmap', {
            get() { return bitmap; },
            set(value) { bitmap = value; }
        });

    }
}