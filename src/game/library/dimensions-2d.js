/** 
 * Arguments for when the dimensions have changed.
 * @typedef DimensionsArgs 
 * @property {Object} previous - The previous dimensions.
 * @property {number} previous.width - Previous width.
 * @property {number} previous.height - Previous height.
 * @property {Object} new - The new dimensions.
 * @property {number} new.width - New width.
 * @property {number} new.height - New height.
*/

/**
 * Two dimensional width and height. 
 * @property {number} width - Width of the dimension. 
 * @property {number} height - Height of the dimension.
 */
export default class Dimensions2D {

    /**
     * Fired when the value of the dimension has changed. 
     * @param {Dimensions2D} sender - The dimension object that the event originated from.
     * @param {DimensionsArgs} args - Event information.
     */
    onchange(sender, args) { 
    }

    /** @type {number} */
    get width() { return 0; }
    set width(value) { }

    /** @type {number} */
    get height() { return 0; }
    set height(value) { }

    constructor(width, height) {

        if (!width || width === null) width = 0;
        Object.defineProperty(this, 'width', {
            get: () => { return width; },
            set: (value) => {
                if (value != width) {
                    let args = { previous: { width: width, height: height }, new: { width: value, height: height } };
                    width = value;
                    this.onchange(this, args);
                }
            }
        });

        if (!height || height === null) height = 0;
        Object.defineProperty(this, 'height', {
            get: () => { return height; },
            set: (value) => {
                if (value != height) {
                    let args = { previous: { width: width, height: height }, new: { width: width, height: value } };
                    height = value;
                    this.onchange(this, args);
                }
            }
        });

    }
}
