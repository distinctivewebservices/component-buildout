import BoundingBox from './../library/bounding-box';
import Collidable from './../attributes/collidable';
import GameObject from './../game-objects/game-object';
import GameObjectCollisionHelper from './helper-collisions-game-object';
import Helpers from './helpers';

/**
 * Provides utility methods for discovering and dealing with collisions. 
 * @module GameEngine/CollisionsHelper 
 */

export default class This {

    /**
     * Collision helpers 
     */
    static get GameObject() { return GameObjectCollisionHelper; }

    /**
     * @typedef collisionResultCallback
     * @property {boolean} overlapping - Are the elements colliding?
     * @property {boolean} overlapX - Does the X coordinate overlap?
     * @property {boolean} overlapY - Does the Y coordinate overlap?
     */

    /**
     * @typedef collisionResultArgs
     * @property {boolean} overlapping - Are the elements colliding?
     * @property {boolean} overlapX - Does the X coordinate overlap?
     * @property {boolean} overlapY - Does the Y coordinate overlap?
     */

    /**
     * Take a collidable object, perform a function on it.
     * @param {Collidable} first - First collidable object to test. 
     * @returns {ifThisObjectResult}
     */
    static if(first) {
        /**
         * @typedef ifThisObjectResult
         * @property {overlapsWithResult} overlapsWith 
         */
        return {
            /**
             * Test whether the first object collides with the second object?
             * @param {Collidable} second - The second collidable object.
             * @returns {overlapsWithResult}
             */
            overlapsWith(second) {
                let overlapX = This.areOverlappingX(first, second);
                let overlapY = This.areOverlappingY(first, second);
                let overlapping = (overlapX && overlapY);
                /**
                 * @typedef overlapsWithResult
                 * @property {collidesWithCallback} then 
                 */
                return {
                    /**
                     * Callback that is fired when the two elements have collided. 
                     * @param {collisionResultArgs} callback - Fire this if the two elements collide.
                     * @returns {collidingWithThenResult}
                     */
                    then(callback) {
                        if (overlapping && callback) callback({ overlapX: overlapX, overlapY: overlapY, overlapping: overlapping });
                        /**
                         * @typedef collidingWithOtherwiseResult
                         * @property {collidesWithCallback} otherwise 
                         */
                        return {
                            /**
                             * Callback that is fired when the two elements haven't collided. 
                             * @param {collisionResultArgs} callback - Fire this if the two elements collide.
                             */
                            otherwise(callback) {
                                if (overlapping && callback) callback({ overlapX: overlapX, overlapY: overlapY, overlapping: overlapping });
                            }
                        }
                    },
                    otherwise(callback) {
                        if (overlapping && callback) callback({ overlapX: overlapX, overlapY: overlapY, overlapping: overlapping });
                    }
                }
            }
        }
    }


    /**
     * Tests to see if the X coordinates of the two objects overlap. 
     * @param {Collidable} collidableA - The first collidable to test.
     * @param {Collidable} collidableB - The second collidable to test. 
     * @returns {boolean} - true if they're overlapping, otherwise false. 
     */
    static areOverlapping(collidableA, collidableB) {
        return areOverlappingX(collidableA, collidableB) && areOverlappingY(collidableA, collidableB);
    }

    /**
     * Tests to see if the X coordinates of the two objects overlap. 
     * @param {Collidable} collidableA - The first collidable to test.
     * @param {Collidable} collidableB - The second collidable to test. 
     * @returns {boolean} - true if they're overlapping, otherwise false. 
     */
    static areOverlappingX(collidableA, collidableB) {
        return collidableA.absoluteRight >= collidableB.absoluteLeft && collidableB.absoluteRight >= collidableA.absoluteLeft;
    }

    /**
     * Tests to see if the Y coordinates of the two objects overlap. 
     * @param {Collidable} collidableA - The first collidable to test.
     * @param {Collidable} collidableB - The second collidable to test. 
     * @returns {boolean} - true if they're overlapping, otherwise false. 
     */
    static areOverlappingY(collidableA, collidableB) {
        return collidableA.absoluteBottom >= collidableB.absoluteTop && collidableB.absoluteBottom >= collidableA.absoluteTop;
    }


};