import Helpers from './helpers';
import GameEngine from './../game-engine';
import Renderable from './../attributes/renderable';
import Collidable from './../attributes/collidable';
import Attribute from './../attributes/attribute';

/**
 * Provides rendering functionality to the game engine. 
 */
export default class Renderer {

    /**
     * Initialises a new instance of the Renderer object. 
     * @param {GameEngine} game - Parent game engine.
     * @param {HTMLCanvasElement} canvas - The canvas to render the game to.
     */
    constructor(game, canvas) {

        let $this = this;
        let $game = game;
        let $canvas = canvas;
        let $ctx = $canvas.getContext('2d');


        /**
         * Renders the game contents to the supplied canvas. 
         * @param {Renderable[]} renderables - Renderable items. 
         * @param {HTMLCanvasElement} [newCanvas] - Optional canvas element that replaces the existing canvas element. 
         */
        this.render = (renderables, newCanvas) => {
            if (newCanvas) {
                $canvas = newCanvas;
                $ctx = $canvas.getContext('2d');
            }
            clearPlayArea(); 
            renderables.forEach(renderable => {
                renderObject(renderable);
            });
        };

        /**
         * Clears the play area so that a new frame can be rendered. 
         */
        let clearPlayArea = () => {
            $ctx.clearRect(0, 0, $canvas.width, $canvas.height);
        }

        /**
         * @param {Renderable} renderable - The object to render.
         */
        let renderObject = (renderable) => {
            drawRenderableObject(renderable);
            renderObjectBoundingIfEnabled(renderable);
            renderCollisionBoxIfEnabled(renderable);
            renderAnchorPositionIfEnabled(renderable);
            resetCanvasTransformation();
        };

        /**
         * Renders an object to the canvas. 
         * @param {Renderable} renderable - The renderable object to draw.
         */
        let drawRenderableObject = (renderable) => {
            let frameBitmap = renderable.getFrame(); // The reason that we get the frame here is because the object shoudl continue to animate in the background.
            if (renderable.visible) {
                let boundingBox = renderable.gameObject.boundingBox;
                Helpers.Canvas.shiftToAnchorPosition(boundingBox, renderable.anchor, $ctx, () => {
                    Helpers.Canvas.shiftRotation(renderable.rotation, $ctx, () => {
                        $ctx.drawImage(frameBitmap, -renderable.anchor.x, -renderable.anchor.y);
                    });
                });
            }
        };

        /**
         * Renders the object's bounding box if it is enabled in debug options. 
         * @param {Renderable} renderable - The renderable object to draw.
         */
        let renderObjectBoundingIfEnabled = (renderable) => {
            if ($game.debugOptions.drawBounding) {
                let boundingBox = renderable.gameObject.boundingBox;
                Helpers.Canvas.shiftFillStyle('transparent', $ctx, () => {
                    Helpers.Canvas.shiftStrokeStyle('blue', $ctx, () => {
                        $ctx.strokeRect(boundingBox.positionX, boundingBox.positionY, boundingBox.width, boundingBox.height);
                    });
                });
            }
        };

        /**
         * Renders the object's collision box if it is enabled in debug options. 
         * @param {Renderable} renderable - The renderable object to draw.
         */
        let renderCollisionBoxIfEnabled = (renderable) => {
            if ($game.debugOptions.drawCollisionBoxes && renderable.gameObject.hasAttribute(Attribute.Names.collidable)) {
                /** @type {Collidable} */
                let collidable = renderable.gameObject.getAttribute(Attribute.Names.collidable);
                Helpers.Canvas.shiftFillStyle('transparent', $ctx, () => {
                    Helpers.Canvas.shiftStrokeStyle('yellow', $ctx, () => {
                        $ctx.strokeRect(
                            collidable.absoluteLeft,
                            collidable.absoluteTop,
                            collidable.boundingBox.width,
                            collidable.boundingBox.height);
                    });
                });
            }
        };

        /**
         * Renders the object's anchor position if it is enabled in debug options. 
         * @param {Renderable} renderable - The renderable object to draw.
         */
        let renderAnchorPositionIfEnabled = (renderable) => {
            if ($game.debugOptions.drawAnchors) {
                let box = renderable.gameObject.boundingBox;
                let anchor = renderable.anchor;
                Helpers.Canvas.shiftToAnchorPosition(box, anchor, $ctx, () => {
                    Helpers.Canvas.shiftFillStyle('red', $ctx, () => {
                        $ctx.fillRect(-1, -1, 2, 2);
                    });
                });
            }
        };

        /**
         * Renders the object's anchor position if it is enabled in debug options. 
         * @param {Renderable} renderable - The renderable object to draw.
         */
        let renderFpsIfEnabled = (renderable) => {
            if ($game.debugOptions.drawFps) {
                Helpers.Canvas.shiftFillStyle('black', $ctx, () => {
                    Helpers.Canvas.shiftStrokeStyle('white', $ctx, () => {
                        Helpers.Canvas.shiftFont('normal 11px sans-serif', $ctx, () => {
                            $ctx.fillRect(0, 0, 50, 14);
                            $ctx.strokeText(Math.round(fpsCount) + ' fps', 2, 10);
                        });
                    });
                });
            }
        };

        /**
         * Returns the canvas back to its original state. 
         */
        let resetCanvasTransformation = () => {
            $ctx.setTransform(1, 0, 0, 1, 0, 0);
        };

    }

}