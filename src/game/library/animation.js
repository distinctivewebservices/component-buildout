import AnimationFrame from './animation-frame';

/**
 * @summary Represents an animation. 
 * @typedef AnimationProps 
 * @property {number} playSpeed - Play speed of the animation, 1 = 100%. 
 * @property {AnimationFrame[]} frames - Collection of animation frame definitions. 
 */

/**
 * @summary Represents an animation.
 * @property {number} playSpeed - Factor which to adjust playback speed, 1 = normal, 0 = pause, 2 = double, etc.
 */
export default class Animation {


    /** @type {number} */
    get playSpeed() { }
    set playSpeed(value) { }


    /**
     * @param {AnimationProps} props 
     */
    constructor(props) {


        let currentFrameIndex = 0;
        let nextFrameDue = Date.now();
        let frames = (props && props.frames) ? props.frames : [];


        let playSpeed = (props && props.playSpeed) ? props.playSpeed :  1;
        Object.defineProperty(this, 'playSpeed', {
            get() { return playSpeed; },
            set(value) { playSpeed = value; }
        });


        /**
         * @summary Is the next frame of the animation due to be rendered? 
         * @returns {boolean}.
         */
        let isFrameDue = () => {
            return Date.now() >= nextFrameDue;
        }

        /**
         * @summary Returns the current animation frame that should be rendered. 
         * @returns {AnimationFrame} The animation frame.
         */
        this.getCurrentFrame = () => {
            if (isFrameDue()) moveNextFrame();
            return frames[currentFrameIndex];
        }

        /**
         * @summary Returns all animation frames.
         * @returns {AnimationFrame[]} The animation frame.
         */
        this.getFrames = () => {
            return frames; 
        }

        /**
         * @summary Progresses to the next animation frame.
         */
        let moveNextFrame = () => {
            if (playSpeed > 0) {
                let thisFrame = frames[currentFrameIndex];
                // Set which frame to play from the array 
                if (thisFrame.nextFrame !== null && thisFrame.nextFrame >= 0 && thisFrame.nextFrame < frames.length) 
                    currentFrameIndex = thisFrame.nextFrame; // Next frame specified in looping animation 
                else if (thisFrame.nextFrame !== null && thisFrame.nextFrame < 0)
                    currentFrameIndex = 0;
                else if (thisFrame.nextFrame !== null && thisFrame.nextFrame >= frames.length)
                    currentFrameIndex = (frames.length - 1);
                else if (currentFrameIndex < frames.length - 1)
                    currentFrameIndex++;
                else
                    currentFrameIndex = 0;
                // Move the due date forward for the next frame
                let frame = frames[currentFrameIndex];
                if (Date.now() - nextFrameDue > 500)
                    nextFrameDue = Date.now() + (frame.timeMs * playSpeed);
                else while (nextFrameDue < Date.now())
                    nextFrameDue += (frame.timeMs * playSpeed);
            }

        }

        /**
         * @summary Resets the state of the animation back to default (excluding speed). 
         */
        this.reset = () => {
            currentFrameIndex = 0;
            nextFrameDue = Date.now();
        }

    }
}