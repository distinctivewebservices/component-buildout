export default {

    /**
     * Creates a new image from a portion of another image.
     * @param {(HTMLCanvasElement|HTMLImageElement|ImageBitmap)} sourceImage - Source image to cut from.
     * @param {BoundingBox} boundingBox - Bounding box that contains the cutting criteria. 
     * @return {(Promise<(ImageBitmap|HTMLCanvasElement|HTMLImageElement)>)} Promising with image object containing the cropped data.
     */
    cropImageWithBounds(sourceImage, boundingBox) {
        return this.extractImagePortion(sourceImage, boundingBox.positionX, boundingBox.positionY, boundingBox.width, boundingBox.height);
    },
    /**
     * Creates a new image from a portion of another image.
     * @param {(HTMLCanvasElement|HTMLImageElement|ImageBitmap)} sourceImage - Source image to cut from.
     * @param {number} [mapOffsetX] - Horizontal offset to begin cutting the image data. 
     * @param {number} [mapOffsetY] - Vertical offset to begin cutting the image data. 
     * @param {number} [width] - Width of the output image. 
     * @param {number} [height] - Height of the output image. 
     * @return {(Promise<(ImageBitmap|HTMLCanvasElement|HTMLImageElement)>)} Promising with image object containing the cropped data.
     */
    cropImage(sourceImage, mapOffsetX, mapOffsetY, width, height) {
        // Fill default values if parameters not supplied. 
        if (!mapOffsetX) mapOffsetX = 0;
        if (!mapOffsetY) mapOffsetY = 0;
        if (!width && width !== 0) width = sourceImage.width;
        if (!height && height !== 0) height = sourceImage.height;
        // Create the bitmap data 
        if (window.createImageBitmap && ImageBitmap) {
            // Browsers that support createImageBitmap 
            return window.createImageBitmap(sourceImage, mapOffsetX, mapOffsetY, width, height);
        }
        else {
            // Other browsers get a canvas element
            return new Promise((resolve, reject) => {
                let canv = document.createElement('canvas');
                canv.width = width;
                canv.height = height;
                let ctx = canv.getContext('2d');
                ctx.drawImage(sourceImage, mapOffsetX, mapOffsetY, width, height, 0, 0, width, height);
                resolve(canv);
            });
        }
    },
    /**
     * Provides methods for determining image scale factors. 
     */
    scaleFactor: {
        /**
         * Gets the percentage to proportionally scale an image based on a 'fit to size' scale. 
         * @arg {number} sourceWidth - Width of the source image that is to be scaled. 
         * @arg {number} sourceHeight - Height of the source image that is to be scaled. 
         * @arg {number} destinationWidth - Width of the destination image that the source is to be drawn onto. 
         * @arg {number} destinationHeight - Height of the destination image that the source is to be drawn onto. 
         * @returns {number} Scale percent to be used for scaling the image dimensions. 
         */
        getProportionalFitFactor(sourceWidth, sourceHeight, destinationWidth, destinationHeight) {
            /** 
             * @todo This method returns wrong results. 
             */
            let destinationIsLandscape = destinationWidth > destinationHeight;
            let sourceIsLandscape = sourceWidth > sourceHeight;
            if ((destinationIsLandscape && sourceIsLandscape) || (!destinationIsLandscape && sourceIsLandscape)) {
                return 1 / sourceWidth * destinationWidth;
            }
            else {
                return 1 / sourceHeight * destinationHeight;
            }
        },
        /**
         * Gets the percentage to proportionally scale an image based on a 'proportionally fill and crop' scale. 
         * @arg {number} sourceWidth - Width of the source image that is to be scaled. 
         * @arg {number} sourceHeight - Height of the source image that is to be scaled. 
         * @arg {number} destinationWidth - Width of the destination image that the source is to be drawn onto. 
         * @arg {number} destinationHeight - Height of the destination image that the source is to be drawn onto. 
         * @returns {number} Scale percent to be used for scaling the image dimensions. 
         */
        getCropFitFactor(sourceWidth, sourceHeight, destinationWidth, destinationHeight) {
            let destinationIsLandscape = destinationWidth > destinationHeight;
            let sourceIsLandscape = sourceWidth > sourceHeight;
            if (!sourceIsLandscape && destinationIsLandscape) {
                return 1 / sourceWidth * destinationWidth;
            }
            else if (sourceIsLandscape && !destinationIsLandscape) {
                return 1 / sourceHeight * destinationHeight;
            }
            else {
                let sourceWidthToHeight = 1 / sourceHeight * sourceWidth;
                let destinationWidthToHeight = 1 / destinationHeight * destinationWidth;
                if (sourceWidthToHeight > destinationWidthToHeight)
                    return 1 / sourceHeight * destinationHeight;
                else
                    return 1 / sourceWidth * destinationWidth;
            }
        }
    }

};