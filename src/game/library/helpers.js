import Collidable from './../attributes/collidable';
import BoundingBox from './bounding-box';
import BoundingBoxHelper from './helper-bounding-box'; 
import CollisionHelper from './helper-collisions';
import CanvasHelper from './helper-canvas';
import ImageHelper from './helper-image';
import MathHelper from './helper-math';

export default {
    isUndefined(value) {
        let undefined = void (0);
        return value === undefined;
    },
    Math: MathHelper,
    Image: ImageHelper,
    Canvas: CanvasHelper,
    Collisions: CollisionHelper, 
    BoundingBox: BoundingBoxHelper 
};