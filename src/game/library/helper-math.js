export default class This {

    /**
     * Converts a measurement in degrees to Radians. 
     * @param {number} degrees - Value in degrees to convert.
     */
    static toRadian(degrees) {
        return degrees * Math.PI / 180;
    }

    /**
     * Is the number between these two values? (value >= lowerValue && value < upperValue)
     * @param {number} value - The value to test.
     * @param {number} firstValue - First number of the range.
     * @param {number} secondValue - Second value for the range.
     * @returns {boolean} - true if the value falls within the range.
     */
    static isBetween(value, firstValue, secondValue) {
        if (firstValue <= secondValue)
            return value >= firstValue && value < secondValue;
        else
            return value >= secondValue && value < firstValue;
    }

    /**
     * Take a value to perform calculations on it. 
     * @param {(number|Date|String)} value - Value to compare.
     * @returns {MathIfResult}
     */
    static if(value) {
        return {
            /** Test the given value to fall between two other values. 
             * @param {(number|Date|String)} firstValue - First value in the range. */
            isBetween(firstValue) {
                return isBetweenTest(firstValue, true);
            },
            /** Test the given value to NOT fall between two other values. 
             * @param {(number|Date|String)} firstValue - First value in the range. */
            notBetween(firstValue) {
                return isBetweenTest(firstValue, false);
            }
        }
    }

};

let isBetweenTest = (firstValue, lookingForBetween) => {
    return {
        /** Pass the second value for testing. 
         * @param {(number|Date|String)} secondValue - Second value in the range. */
        and(secondValue) {
            let result = This.isBetween(value, firstValue, secondValue);
            if (!lookingForBetween) result = !result; 
            return {
                /** Executes a callback or returns a value indicating whether the test returned true or not.
                 * @param {MathGenericCallback} [callback] - Omit the callback if you just want a boolean result. */
                then(callback) {
                    if (callback && result) callback();
                    return {
                        /** Execite this callback if the condition being evaluated was not met.
                         * @param {MathGenericCallback} callback */
                        otherwise(callback) {
                            if (callback && !result) callback();
                        }
                    }
                }, 
                /** Returns the result of the comparison operation. */
                value: result 
            }
        }
    }
}

/**
 * @typedef MathIfResult 
 * @property {MathIfIsBetweenResult} isBetween 
 */
/**
 * @typedef MathIfIsBetweenResult 
 * @property {MathIsIsBetweenAndResult} and 
 */
/**
 * @typedef MathIsIsBetweenAndResult 
 * @property {MathIsIsBetweenAndThenResult} then 
 * @property {boolean} value 
 */
/**
 * @typedef MathIsIsBetweenAndThenResult 
 * @property {MathIsIsBetweenAndThenOtherwiseResult} otherwise 
 */
/**
 * Callback to execute.
 * @callback MathGenericCallback
 */
