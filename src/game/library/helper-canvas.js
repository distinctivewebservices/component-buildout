export default class This {

        /**
         * Temporarily shifts the anchor position
         * @param {BoundingBox} boundingBox 
         * @param {Point2D} anchor 
         * @param {ConvasRenderingContext2D} context 
         * @param {function} callback 
         */
        static shiftToAnchorPosition(boundingBox, anchor, context, callback) {
            let trnsX = boundingBox.positionX + anchor.x;
            let trnsY = boundingBox.positionY + anchor.y;
            context.translate(trnsX, trnsY);
            callback();
            context.translate(-trnsX, -trnsY);
        }

        static shiftRotation(rotation, context, callback) {
            let radians = (rotation * Math.PI / 180);
            context.rotate(radians);
            callback();
            context.rotate(-radians);
        }

        static shiftFillStyle(fillStyle, context, callback) {
            let oldFill = context.fillStyle;
            context.fillStyle = fillStyle;
            callback();
            context.fillStyle = oldFill;
        }

        static shiftStrokeStyle(strokeStyle, context, callback) {
            let oldStroke = context.strokeStyle;
            context.strokeStyle = strokeStyle;
            callback();
            context.strokeStyle = oldStroke;
        }

        static shiftFont(font, context, callback) {
            let oldFont = context.font;
            context.font = font;
            callback();
            context.strokeStyle = oldFont;
        }
        
}
