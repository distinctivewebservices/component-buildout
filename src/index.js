import './css/styles.scss';
import './index.html';
import Vue from 'vue';

import Attribute from './game/attributes/attribute';
import GameEngine from './game/game-engine';
import LabelGameObject from './game/game-objects/label-game-object';
import PlayerGameObject from './game/game-objects/player-game-object';
import PlatformGameObject from './game/game-objects/platform-game-object';
import Controller from './game/controllers/controller'
import KeyboardController from './game/controllers/keyboard'

let lblScoreText = new LabelGameObject({
    text: 'Score',
    positionX: 10,
    positionY: 10
});

let lblScore = new LabelGameObject({
    text: '0',
    positionX: 150,
    positionY: 10
});

let player = new PlayerGameObject({ positionX: 100, positionY: 150, width: 100, height: 100, zIndex: 100 });

let platform1 = new PlatformGameObject({ positionX: 20, positionY: 250, width: 200, height: 200, zIndex: 99 });
let platform2 = new PlatformGameObject({ positionX: 300, positionY: 250, width: 200, height: 200, zIndex: 99 });

let CONT = Controller;
let controller = new KeyboardController();
controller.defineCommand(Controller.defaultCommands.jump, ' ');
controller.defineCommand(Controller.defaultCommands.moveLeft, 'ArrowLeft');
controller.defineCommand(Controller.defaultCommands.moveRight, 'ArrowRight');
// controller.oncommand = (command) => {
//     console.log(`Controller.oncommand: ${command}`);
//     switch (command) {
//         case CONT.defaultCommands.jump:
//             player.movable.velocityY = -250;
//             break;
//         case CONT.defaultCommands.moveLeft:
//             player.movable.velocityX = -250;
//             break;
//         case CONT.defaultCommands.moveRight:
//             player.movable.velocityX = 250;
//             break;
//     }
// };
controller.onCommand((command) => {
    console.log(`Controller.oncommand: ${command}`);
    switch (command) {
        case CONT.defaultCommands.jump:
            player.movable.velocityY = -250;
            break;
        // case CONT.defaultCommands.moveLeft:
        //     player.movable.velocityX = -250;
        //     break;
        // case CONT.defaultCommands.moveRight:
        //     player.movable.velocityX = 250;
        //     break;
    }
});

let gameEngine = new GameEngine(document.querySelector("#gameCanvas"));
gameEngine.debugOptions.drawAnchors = true;
gameEngine.debugOptions.drawBounding = true;
gameEngine.debugOptions.drawCollisionBoxes = true;
gameEngine.debugOptions.drawFps = true;
gameEngine.addGameObjects([lblScoreText, lblScore, player, platform1, platform2]);
gameEngine.addController(controller);
gameEngine.startGame();
