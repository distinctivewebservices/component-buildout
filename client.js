const path = require('path');
const express = require('express');
const webpack = require('webpack'); 

const app = express();

const HOSTNAME = 'localhost'; 
const PORT = 8081; 

app.set('port', PORT);

app.use('/', express.static('dist')); 

var server = app.listen(app.get('port'), function () {
    var port = server.address().port;
    console.log(`Listening on http://${HOSTNAME}:${PORT}`);
});